%MATLAB2020b script
%name: task_2_cynddan.m
%author: Daniel "cynddan" Cyndecki
%date: 22.11.2020
%version: v1.0.4


datesFromFiles = cell(1,14);
hourNO2 = (0);
valueNO2 = (0);
lvlExcNO2h = '';
lvlExcPM1024h = '';
lvlExcCO8h = '';


%loading data from files
for i = 1:14
    date = num2str(i+1, '%02d');
    filePath = string(['dane-pomiarowe_2020-11-', date, '.csv']);
    fileRead = readmatrix(filePath);
    datesFromFiles{i} = fileRead(1:24,3:end);
end



%checking level exceeded for NO2 per h
for i = 1:14
    dailyLvls = cell2mat(datesFromFiles(i));
    if sum(dailyLvls(:,1)>200)>0
        [hourNO2, valueNO2] = dailyLvls(dailyLvls(:,1)>200);
        lvlExcNO2h = [lvlExcNO2h, num2str(hourNO2), num2str(valueNO2)];
    end
end



%checking level exceeded for PM10 per 24h
for i = 1:14
    dailyLvls = cell2mat(datesFromFiles(i));
    date = num2str(i+1, '%02d');
    if i == 14
        if ~any(isnan(dailyLvls(:,8)))
                avgPM10 = sum(dailyLvls(:,8))/24;
            else
                dailySum = 0;
                n = 24;
                for j = 1:24
                    if isnan(dailyLvls(j,8))
                        n = n-1;
                    else
                        dailySum = dailySum + dailyLvls(j,8);
                    end
                    avgPM10 = dailySum/n;
                end
        end
        if avgPM10 > 50
             lvlExcPM1024h = [lvlExcPM1024h, date,'.11-', date,'.11 - 1:00-24:00 - ', num2str(avgPM10),'   '];
        end
    else
        dailyLvls2 = cell2mat(datesFromFiles(i+1));
        date2 = num2str(i+2, '%02d');
        for k = 1:24
            if k==1
                if ~any(isnan(dailyLvls(:,8)))
                    avgPM10 = sum(dailyLvls(:,8))/24;
                else
                    dailySum = 0;
                    n = 24;
                    for j = 1:24
                        if isnan(dailyLvls(j,8))
                            n = n-1;
                        else
                            dailySum = dailySum + dailyLvls(j,8);
                        end
                        avgPM10 = dailySum/n;
                    end
                end
                if avgPM10 > 50
                    lvlExcPM1024h = [lvlExcPM1024h, date,'.11-', date,...
                        '.11 - 1:00-24:00 - ', num2str(avgPM10),'   '];
                end
            else
                if (~any(isnan(dailyLvls(k:end,8))) && ~any(isnan(dailyLvls2(1:k,8))))
                    avgPM10 = (sum(dailyLvls(k:end,8)) + sum(dailyLvls2(1:k,8)))/24;
                else
                    n = 24;
                    dailySum = 0;
                    for j = 1:24
                        if j >= k
                            if ~isnan(dailyLvls2(j,8))
                                dailySum = dailySum + dailyLvls2(j,8);
                            else
                                n = n-1;
                            end
                        else
                            if ~isnan(dailyLvls(end-j,8))
                                dailySum = dailySum + dailyLvls(end-j,8);
                            else
                                n = n-1;
                            end
                        end
                        avgPM10 = dailySum/n;
                    end
                end
            end
            if avgPM10>50
                lvlExcPM1024h = [lvlExcPM1024h, date,'.11-', date2,'.11 - ',...
                    num2str(k, '%02d'),':00-',num2str(k-1, '%02d'),':00 - ',...
                    num2str(avgPM10),'   '];
            end
        end
    end
end



%checking for level exceeded for CO per 8h
for i = 1:14
    dailyLvls = cell2mat(datesFromFiles(i));
    date = num2str(i+1, '%02d');
    if i == 14
        for j = 1:24
            if j<=16
                if ~any(isnan(dailyLvls(j:j+8,6)))
                    avgEightH = sum(dailyLvls(j:j+8,6))/8;
                else
                    for k = 1:8
                        n = 8;
                        eightHSum = 0;
                        if isnan(dailyLvls(j+k,6))
                            n = n - 1;
                        else
                            eightHSum = eightHSum + dailyLvls(j+k,6);
                        end
                        avgEightH = eightHSum/n;
                    end
                end
            end
        end
        if avgEightH>10000
            lvlExcCO8h = [lvlExcCO8h, date,'.11-', date2,'.11 - ',...
                num2str(k, '%02d'),':00-',num2str(k-1, '%02d'),':00 - ',...
                num2str(avgPM10),'\n'];
        end
    else
        for j = 1:24
            if j<=16
                if ~any(isnan(dailyLvls(j:j+8,6)))
                    avgEightH = sum(dailyLvls(j:j+8,6))/8;
                else
                    for k = 1:8
                        n = 8;
                        eightHSum = 0;
                        if isnan(dailyLvls(j+k,6))
                            n = n - 1;
                        else
                            eightHSum = eightHSum + dailyLvls(j+k,6);
                        end
                        avgEightH = eightHSum/n;
                    end
                end
            else
                dailyLvls2 = cell2mat(datesFromFiles(i+1));
                date2 = num2str(i+2, '%02d');
                for k = 1:8
                    if (~any(isnan(dailyLvls(j:end,6))) && ~any(isnan(dailyLvls2(1:k,6))))
                        avgEightH = (sum(dailyLvls(j:end,6)) + sum(dailyLvls2(1:k,6)))/8;
                    else
                        if isnan(dailyLvls(j+k,6))
                             n = n - 1;
                        else
                            eightHSum = eightHSum + dailyLvls(j+k,6)
                        end
                        if isnan(dailyLvl2(k,6))
                            n = n -1;
                        else
                            eightHSum = eightHSum + dailyLvls2(j+k,6)
                        end
                        avgEightH = eightHSum/n;
                    end
                end
            end
            if avgEightH>10000
                lvlExcCO8h = [lvlExcCO8h, date,'.11-', date2,'.11 - ',...
                    num2str(k, '%02d'),':00-',num2str(k+8, '%02d'),':00 - ',...
                    num2str(avgPM10),'\n'];
            end
        end
    end
end



%saving data about exceeded levels
outputFile = fopen('PMresults.dat', 'w+');
fprintf("%s",lvlExcCO8h)
fprintf("%s",lvlExcNO2h)
fprintf("%s",lvlExcPM1024h)
fclose('all');















